/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.shapeproject1;

/**
 *
 * @author admin
 */
public class Triangle extends Shape {

    private double Height;
    private double Base;

    public Triangle(double Height, double Base) {
        super("Triangle");
        this.Height = Height;
        this.Base = Base;
    }

    public double getHeight() {
        return Height;
    }

    public void setHeight(double Height) {
        this.Height = Height;
    }

    public double getBase() {
        return Base;
    }

    public void setBase(double Base) {
        this.Base = Base;
    }
    
    @Override
    public double calArea() {
        return (Height * Base) / 2;
    }

    @Override
    public double calPerimeter() {
        return Math.sqrt((Math.pow(Height, 2) + Math.pow(Base, 2))) + Height + Base;
    }
}
