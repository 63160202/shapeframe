/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.shapeproject1;

/**
 *
 * @author admin
 */
public class Rectangle extends Shape {
    private double Width;
    private double Height;

    public Rectangle(double Width, double Height) {
        super("Rectangle");
        this.Width = Width;
        this.Height = Height;
    }

    public double getWidth() {
        return Width;
    }

    public void setWidth(double Width) {
        this.Width = Width;
    }

    public double getHeight() {
        return Height;
    }

    public void setHeight(double Height) {
        this.Height = Height;
    }
    
    @Override
    public double calArea() {
        return Width * Height;
    }

    @Override
    public double calPerimeter() {
        return 2 * (Width + Height);
    }

}
