/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.shapeproject1;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author admin
 */
public class TriangleFrame extends JFrame {

    JLabel lblbase;
    JLabel lblHeight;
    JTextField txtbase;
    JTextField txtHeight;
    JButton btnCalculate;
    JLabel lblResult;

    public TriangleFrame() {
        super("Triangle");
        this.setSize(350, 300);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);

        lblbase = new JLabel("Base: ", JLabel.TRAILING);
        lblbase.setSize(50, 20);
        lblbase.setLocation(5, 5);
        lblbase.setBackground(Color.WHITE);
        lblbase.setOpaque(true);
        this.add(lblbase);

        lblHeight = new JLabel("Height: ", JLabel.TRAILING);
        lblHeight.setSize(55, 25);
        lblHeight.setLocation(5, 35);
        lblHeight.setBackground(Color.WHITE);
        lblHeight.setOpaque(true);
        this.add(lblHeight);

        txtbase = new JTextField();
        txtbase.setSize(60, 20);
        txtbase.setLocation(65, 5);
        this.add(txtbase);

        txtHeight = new JTextField();
        txtHeight.setSize(60, 20);
        txtHeight.setLocation(65, 40);
        this.add(txtHeight);

        btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(150, 5);
        this.add(btnCalculate);

        lblResult = new JLabel("base= ??? Height= ??? area= ??? perimeter=???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(350, 50);
        lblResult.setLocation(0, 100);
        lblResult.setBackground(Color.CYAN);
        lblResult.setOpaque(true);
        this.add(lblResult);

        btnCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strBase = txtbase.getText();
                    double base = Double.parseDouble(strBase);
                    String strHeight = txtHeight.getText();
                    double height = Double.parseDouble(strHeight);
                    Triangle triangle = new Triangle(base, height);
                    lblResult.setText("Base = " + String.format("%.2f", triangle.getBase())
                            + " Height = " + String.format("%.2f", triangle.getHeight())
                            + " area = " + String.format("%.2f", triangle.calArea())
                            + " perimeter = " + String.format("%.2f", triangle.calPerimeter()));
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(TriangleFrame.this, "Error: Please input number", "Errror",
                            JOptionPane.ERROR_MESSAGE);
                    txtbase.setText("");
                    txtbase.requestFocus();
                    txtHeight.setText("");
                    txtHeight.requestFocus();
                }
            }
        });

    }

    public static void main(String[] args) {
        TriangleFrame frame = new TriangleFrame();
        frame.setVisible(true);
    }

}
